extern crate rust_pinnacle_api;
extern crate url;
extern crate hyper;
extern crate rand;
#[macro_use]
extern crate lazy_static;

use std::io;
use std::fs::File;
use hyper::error::{Result,Error};
use hyper::server::{Handler,Server,Listening,Request,Response};
use hyper::uri::RequestUri;
use hyper::method::Method;
use url::Url;
use rust_pinnacle_api::api::Api;
use rust_pinnacle_api::configuration::Config;

pub mod api;
pub mod configuration;

pub fn get_test_server<H: Handler + 'static>(handler: H) -> Result<Listening> {
    let mut server:Result<Server> = std::result::Result::Err(Error::TooLarge);
    for _ in 0..10 {
        let port = 30000 + rand::random::<u8>() as u32; //random port between 30000 and 30255
        server = Server::http(format!("127.0.0.1:{}",port).as_str());
        if server.is_ok() {
            break;
        }

    }
    return server.unwrap().handle_threads(handler,1);
}

pub fn get_test_env<H: Handler + 'static>(handler: H) -> TestEnv {
    let server: Listening = get_test_server(handler).unwrap();
    let url = format!("http://{}:{}",server.socket.ip(),server.socket.port());
    let api = Api{config: Config {
        pinnacle_api_auth: "bla".to_string(),
        pinnacle_api_url: Url::parse(&url).unwrap(),
    }};

    TestEnv {
        api: api,
        server: server
    }
}

pub struct TestEnv {
    api: Api,
    server: Listening
}
impl Drop for TestEnv{
    #[allow(unused_must_use)]
    fn drop(&mut self) {
        self.server.close();
    }
}

pub struct JsonGetHandler {
    url:&'static str,
    file_name:&'static str
}
impl Handler for JsonGetHandler{
    fn handle(&self, req: Request, res: Response) {
        if req.method == Method::Get && req.uri == RequestUri::AbsolutePath(self.url.to_string()) {
            let mut f = File::open(self.file_name).unwrap();
            io::copy(&mut f, &mut res.start().unwrap()).unwrap();
        }
        else {
            panic!("Nope {:?}",req.uri)
        }
    }
}
