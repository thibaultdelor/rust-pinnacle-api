use std::env;
use std::result::Result::{Err, Ok};

use rust_pinnacle_api::configuration::{ConfError, Config, get_conf_from_env};


fn clean_env() {
    env::remove_var("PINNACLE_API_AUTH");
    env::remove_var("PINNACLE_API_URL");
}

// Run all in the same test to avoid race condition since env is shared
#[test]
fn test_configuration() {
    clean_env();

    let fake_pinnacle_api_auth: String = "SOMETHING".to_string();
    let fake_pinnacle_api_url: String = "http://127.0.0.1:3000/".to_string();
    env::set_var("PINNACLE_API_URL", &fake_pinnacle_api_url);


    let mut conf = get_conf_from_env();
    let mut matching = match conf {
        Err(ConfError::MissingVar { variable: ref v }) => v == "PINNACLE_API_AUTH",
        _ => false,
    };
    assert!(matching,
            format!("Check PINNACLE_API_AUTH is required : Unexpected Result {:?}",
                    conf));



    env::set_var("PINNACLE_API_AUTH", &fake_pinnacle_api_auth);
    env::remove_var("PINNACLE_API_URL");
    conf = get_conf_from_env();
    matching = match conf {
        Err(ConfError::MissingVar { variable: ref v }) => v == "PINNACLE_API_URL",
        _ => false,
    };
    assert!(matching,
            format!("Check PINNACLE_API_URL is required : Unexpected Result {:?}",
                    conf));



    env::set_var("PINNACLE_API_URL", "bla");
    conf = get_conf_from_env();
    matching = match conf {
        Err(ConfError::ParseVarError { variable: ref v, value: _, error: _  }) => v == "PINNACLE_API_URL",
        _ => false,
    };
    assert!(matching,
            format!("Check PINNACLE_API_URL has to be an url : Unexpected Result {:?}", conf));


    env::set_var("PINNACLE_API_URL", &fake_pinnacle_api_url);
    conf = get_conf_from_env();
    matching = match conf {
        Ok(Config { ref pinnacle_api_auth, ref pinnacle_api_url })
            => pinnacle_api_auth == &fake_pinnacle_api_auth && pinnacle_api_url.as_str() == &fake_pinnacle_api_url,
        _ => false,
    };
    assert!(matching,
            format!("Check successful configuration : Unexpected Result {:?}", conf));
    clean_env();
}