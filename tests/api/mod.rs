use get_test_env;
use JsonGetHandler;


#[test]
fn test_get_sports() {
    let test_env = get_test_env(JsonGetHandler {
        file_name: "tests/responses/getSports.json",
        url: "/v2/sports"
    });

    let sports = test_env.api.get_sports_v2().unwrap();

    assert_eq!(sports.len(), 55);
    assert_eq!(sports[26].id, 29u64 );
    assert_eq!(sports[26].name, "Soccer" );
    assert_eq!(sports[26].hasOfferings, true );
    assert_eq!(sports[26].leagueSpecialsCount, 8u64 );
    assert_eq!(sports[26].eventSpecialsCount, 278u64 );
    assert_eq!(sports[26].eventCount, 718u64 );


    assert_eq!(sports[24].id, 27u64 );
    assert_eq!(sports[24].name, "Rugby Union" );
    assert_eq!(sports[24].hasOfferings, false );
    assert_eq!(sports[24].leagueSpecialsCount, 0u64 );
    assert_eq!(sports[24].eventSpecialsCount, 0u64 );
    assert_eq!(sports[24].eventCount, 0u64 );
}

#[test]
fn test_get_leagues() {
    let test_env = get_test_env(JsonGetHandler {
        file_name: "tests/responses/getLeagues.json",
        url: "/v2/leagues?sportId=27"
    });
    let leagues = test_env.api.get_leagues_v2(&27).unwrap();

    assert_eq!(leagues.len(), 28);
    assert_eq!(leagues[2].id, 9484u64 );
    assert_eq!(leagues[2].name, "Currie Cup" );
    assert_eq!(leagues[2].homeTeamType, "Team1" );
    assert_eq!(leagues[2].hasOfferings, false );
    assert_eq!(leagues[2].allowRoundRobins, false );
    assert_eq!(leagues[2].leagueSpecialsCount, 0 );
    assert_eq!(leagues[2].eventSpecialsCount, 0 );
    assert_eq!(leagues[2].eventCount, 0 );
}


#[test]
fn test_get_fixtures() {
    let test_env = get_test_env(JsonGetHandler {
        file_name: "tests/responses/getFixtures.json",
        url: "/v1/fixtures?sportId=29&leagueIds=2036,2037,6267"
    });
    let fixtures = test_env.api.get_fixtures(&29, Some(&([2036u64, 2037, 6267])), None, None).unwrap();

    assert_eq!(fixtures.sportId, 29);
    assert_eq!(fixtures.last, 91367262);
    assert_eq!(fixtures.league.len(), 2);
    assert_eq!(fixtures.league[0].id, 2036);
    assert_eq!(fixtures.league[0].events.len(), 25);
    assert_eq!(fixtures.league[0].events[0].id, 667884990);
    assert_eq!(fixtures.league[0].events[0].starts, "2016-11-30T18:00:00Z");
    assert_eq!(fixtures.league[0].events[0].home, "Nancy");
    assert_eq!(fixtures.league[0].events[0].away, "Metz");
    assert_eq!(fixtures.league[0].events[0].rotNum, "3140");
    assert_eq!(fixtures.league[0].events[0].liveStatus, Some(2));
    assert_eq!(fixtures.league[0].events[0].status, "I");
    assert_eq!(fixtures.league[0].events[0].parlayRestriction, 2);
    assert_eq!(fixtures.league[0].events[0].homePitcher, None);
    assert_eq!(fixtures.league[0].events[0].awayPitcher, None);

    assert_eq!(fixtures.league[1].id, 6267);
}