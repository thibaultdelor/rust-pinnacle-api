use std::env;
use std::env::VarError;
use url::Url;


#[derive(Debug)]
pub struct Config {
    pub pinnacle_api_auth: String,
    pub pinnacle_api_url: Url,
}

#[derive(Debug)]
pub enum ConfError {
    MissingVar { variable: String },
    ParseVarError {
        variable: String,
        value: String,
        error: String,
    },
}

fn get_env_var(var_name: &String) -> Result<String, ConfError> {
    env::var(var_name).map_err(|err| match err {
        VarError::NotPresent => ConfError::MissingVar { variable: var_name.clone() },
        VarError::NotUnicode(s) => {
            ConfError::ParseVarError {
                variable: var_name.clone(),
                error: "Not Unicode".to_string(),
                value: s.to_string_lossy().into_owned(),
            }
        }
    })
}

pub fn get_conf_from_env() -> Result<Config, ConfError> {
    let url_var_name = "PINNACLE_API_URL".to_string();
    let pinnacle_api_url_str = try!(get_env_var(&url_var_name));
    let pinnacle_api_auth = try!(get_env_var(&"PINNACLE_API_AUTH".to_string()));
    let pinnacle_api_url = Url::parse(&pinnacle_api_url_str).map_err(|err| ConfError::ParseVarError {
        variable: url_var_name,
        value: pinnacle_api_url_str,
        error: err.to_string(),
    })?;
    Result::Ok(Config {
        pinnacle_api_auth: pinnacle_api_auth,
        pinnacle_api_url: pinnacle_api_url,
    })
}