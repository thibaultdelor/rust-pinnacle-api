mod model;

use serde_json;
use serde;
use hyper::{self, method};
use hyper::client::{Client, Response};
use std::result;
use url;
use configuration::Config;
use std::option::Option;

#[derive(Debug)]
pub enum Error {
    HyperError(hyper::Error),
    SerdeJsonError(serde_json::Error)
}

impl From<hyper::Error> for Error {
    fn from(error: hyper::Error) -> Self {
        Error::HyperError(error)
    }
}

impl From<serde_json::Error> for Error {
    fn from(error: serde_json::Error) -> Self {
        Error::SerdeJsonError(error)
    }
}

impl From<url::ParseError> for Error {
    fn from(error: url::ParseError) -> Self {
        Error::HyperError(hyper::Error::from(error))
    }
}

pub type Result<T> = result::Result<T, Error>;


pub struct Api {
    pub config: Config
}

impl Api {
    fn get<T>(&self, path: &str) -> Result<T>
        where T: for<'de> serde::Deserialize<'de> {
        let url = self.config.pinnacle_api_url.join(path.as_ref())?;

        use hyper::header::Basic;
        use std::str::FromStr;

        use hyper::header::{Headers, Accept, qitem, Authorization};
        use hyper::mime::{Mime, TopLevel, SubLevel};

        let mut reqheaders = Headers::new();
        reqheaders.set(Authorization(Basic::from_str(self.config.pinnacle_api_auth.as_ref()).unwrap()));
        reqheaders.set(Accept(vec![qitem(Mime(TopLevel::Application, SubLevel::Json, vec![]))]));

        let response: Response = Client::new().request(method::Method::Get, url).headers(reqheaders).send()?;

        serde_json::from_reader(response).map_err(Error::from)
    }

    pub fn get_sports_v2(&self) -> Result<Vec<model::Sport>> {
        self.get::<model::SportResponse>("/v2/sports").map(|r| r.sports)
    }

    pub fn get_leagues_v2(&self, sport_id: &u64) -> Result<Vec<model::League>> {
        self.get::<model::LeagueResponse>(format!("/v2/leagues?sportId={}", sport_id).as_ref()).map(|r| r.leagues)
    }
    pub fn get_fixtures(&self, sport_id: &u64, league_ids: Option<&[u64]>, since: Option<u64>, islive: Option<bool>) -> Result<model::FixtureResponse> {
        use std::fmt::Write;
        let mut url: String = String::from("/v1/fixtures?sportId=");
        write!(url,  "{}", sport_id).unwrap();
        if league_ids.is_some() {
            write!(url, "&leagueIds={}", league_ids.unwrap().iter().map(|i| i.to_string()).collect::<Vec<String>>().join(",")).unwrap();
        }
        if since.is_some() {
            write!(url,  "&since={}", since.unwrap()).unwrap();
        }
        if islive.is_some() {
            write!(url, "&islive={}", if islive.unwrap() {1} else {0}).unwrap();
        }

        self.get::<model::FixtureResponse>(url.as_str())
    }
}