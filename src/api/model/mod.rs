#![allow(non_snake_case)]

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct SportResponse {
    pub sports:Vec<Sport>
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Sport {
    pub id:u64,
    pub name: String,
    pub hasOfferings: bool,
    pub leagueSpecialsCount: u64,
    pub eventSpecialsCount:u64,
    pub eventCount:u64
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct LeagueResponse {
    pub leagues:Vec<League>
}
#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct League {
    pub id:u64,
    pub name: String,
    pub homeTeamType: String,
    pub hasOfferings: bool,
    pub allowRoundRobins: bool,
    pub leagueSpecialsCount: u64,
    pub eventSpecialsCount:u64,
    pub eventCount:u64
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct FixtureResponse {
    pub sportId:u64,
    pub last:u64,
    pub league:Vec<LeagueFixture>
}
#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct LeagueFixture {
    pub id:u64,
    pub events:Vec<LeagueEvent>
}
#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct LeagueEvent {
    pub id:u64,
    pub starts: String,
    pub home: String,
    pub away: String,
    pub rotNum: String,
    pub liveStatus: Option<i32>,
    pub status: String,
    pub parlayRestriction: i32,
    pub homePitcher: Option<String>,
    pub awayPitcher:Option<String>
}